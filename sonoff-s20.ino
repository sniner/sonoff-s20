#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>

#include "src/sonoff_s20.h"
#include "src/config.h"
#include "src/button.h"
#include "src/wifi.h"
#include "src/webserver.h"
#include "src/mqtt.h"
#include "src/munin.h"
#include "src/utils.h"

void setup()
{
    device.setup();
    Serial.begin(115200);
    delay(250);

    // Setting default config values
    config.deviceAlias = device.uniqueName();
    prepareMQTT();
    prepareMunin();

    Serial.println("Sonoff S20 Smart Socket");
    Serial.println(String("Unique device name: ")+device.uniqueName());
    Serial.println(String("Free sketch memory: ")+ESP.getFreeSketchSpace());
    Serial.println("");

    delay(100);

    setupButton();

    if (setupConfig())
        setupWifi();

    delay(250);

    MDNS.begin(config.deviceAlias.c_str());

    setupWebServer();
    setupMQTT();
    setupMunin();

    device.ledReset();
}

void loop()
{
    loopButton();
    loopWifi();
    loopWebServer();
    loopMQTT();
    loopMunin();
}

// vim: set et ts=4 sw=4 ft=cpp:
