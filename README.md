# DIY firmware for 'Sonoff S20 Smart Switch'

This is a DIY firmware for Sonoff S20 Smart Switch, which is inexpensive but IMHO a good WiFi controlled outlet. The only downside I can see right now: it switches only one wire. Make sure line wire is switched off, not neutral wire.

The Sonoff S20 incorporates an ESP8266, which is easy to program. This firmware turns it into a great device for home automation:

* Configurable WiFi settings: if it can not connect to a WiFi network, it goes into Access Point mode and waits for configuration.
* MQTT: broker address and topic are both configurable.
* HTTP access: for switching on/off and configuration. No TLS and password protection so far.
* Munin node. _Untested._
* OTA Update: you have to open the device only once, further firmware updates are OTA. _Work in progress._

## How to build

* Arduino IDE (version 1.8.5 or later)
* Install ESP8266 board module
  - Add `http://arduino.esp8266.com/stable/package_esp8266com_index.json` as board manager URL
  - Install "esp8266" (version 2.4.0 or later) via bord manager
  - Select "Generic ESP8266 Module"
  - Set "Flash size" to "1M (no SPIFFS)"
  - Set "Debug port" to "Serial" if you want to see debug messages, "Disabled" for OTA updates
  - Set "Flash mode" to "DOUT"
* Install these sketch libraries
  - PubSubClient by Nick O'Leary (version 2.6.0 or later)
  - WiFiManager by tzapu (version 0.12 or later)

## How to test the firmware

I used a NodeMCU 1.0 (ESP 12-E) for development and testing. One LED represents the button LED, the second the relay. Nothing special, just upload the firmware and it is up and running. Restarting the ESP8266 by software does not work unless you did at least one reset by hand (press the reset button on NodeMCU board).

![](./misc/breadboard-schematic.png)

## How to flash the firmware

**Always disconnect the switch from mains before fiddling with it!**

### First time only

* Open the housing: There are three screws on the back side, one is hidden behind a small label.
* On the PCB you will find a row of four pin header holes. You may solder a pin header onto or - because it is not easy to remove the PCB for soldering - insert a pin header connected to your USB-to-UART-converter and hold it firmly while uploading the firmware.
* **Before** connecting the TTL UART you have to **press and hold the button** next to the soldering pads. This will put the powered on ESP8266 into programming mode. You may release the button after the ESP8266 is powered on and waiting for the upload.
* Upload the firmware.
* After uploading the firmware, remove the connection, close the housing, tighten the screws and you are ready to insert the switch into an outlet.

After the initial firmware upload, there is no configuration on the device. Therefore, it will first open an AP that you can connect to. Connect to the open WiFi AP and select a wireless network. There is also an input field for the device name (must be unique within your LAN).

### OTA update

* Make sure you are using the same Arduino board settings as before (especially flash size).
* Start the upload (Ctrl-U). Of course, it will fail, because there is no ESP8266 connected.
* Locate the firmware file. You may find it at `/tmp/arduino_build_*/sonoff-s20.ino.bin` ([YMMV](https://esp8266.github.io/Arduino/versions/2.0.0/doc/ota_updates/ota_updates.html#web-browser)).
* Open a webbrowser and - assuming your device can be found at 192.168.0.10 - navigate to `http://192.168.0.10/update`.
* Press the button to select the `sonoff-s20.ino.bin` file and press `Update`.

**Please note:** Currently there are no security measurements to prevent unsolicited firmware uploads from within your LAN.

## How to use

Assuming the device is connected to your WiFi and can be found at 192.168.0.10. If you named it `switch1` you should also find it as `switch1.local`, but mDNS doesn't work reliably, in my experience.

### Button

* The obvious first: by pressing the button you switch the outlet on/off. To prevent unintentional operation, hold the button pressed for a short moment (0.3 seconds).
* Pressing the button for ten seconds will restart the device and put it into WiFi configuration mode. After a few seconds the LED will start flashing to indicate the upcoming reset. Release button when LED stopped flashing. Connect to the open WiFi AP and select a wireless network. After configuration the device will restart again.
* _Planned:_ If the button is released while the LED flashes, the OTA update is enabled for five minutes. At the moment I don't know how to disable/enable the OTA update, so this feature is not built in.

### HTTP

* `http://192.168.0.10/`: main web page with information about your device and configuration.
* `http://192.168.0.10/on`: will switch the device on.
* `http://192.168.0.10/off`: will switch the device off.

### MQTT

If you have set the topic to `switch1`, the device will

* subscribe to `switch1` and
* publish to `switch1/status`.

Only the subscribe topic is configurable, the publish topic is derived from the subscribe topic by appending `/status`. The payload is `ON` and `OFF` for both topics.

## Improvements to come (maybe)

* Restoring the previous state after a power failure. (Maybe it is not a good idea to have different topics for pub/sub?)
* Optionally power off when loosing WiFi connectivitiy.
