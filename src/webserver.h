#ifndef __WEBSERVER_H
#define __WEBSERVER_H

void setupWebServer();
void loopWebServer();

void enableOTA();

#endif

// vim: set et ts=4 sw=4:
