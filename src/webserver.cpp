#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServer.h>
#include <ESP8266mDNS.h>

#include "sonoff_s20.h"
#include "webserver.h"
#include "utils.h"
#include "config.h"

static const int webserverPort = 80;
static ESP8266WebServer webserver(webserverPort);
static ESP8266HTTPUpdateServer httpUpdater;

static const unsigned long otaOpenWindow = 5 * 60000L; // ms
static unsigned long otaOpenStart = 0;
static bool otaOpen = false;

static void redirectHome()
{
    webserver.sendHeader("Location", "/", true);
    webserver.send(302, "text/plain", "");
}

static String htmlHeader()
{
    String html = "<html><head>";
    html += "<title>";
    html += device.uniqueName();
    html += "</title>";
    html += "<link rel='stylesheet' href='style.css'></head><body>";
    html += "<div id='header'><b>Sonoff S20 Smart Socket</b> - <a href='https://github.com/sniner/sonoff-s20'>s9r firmware</a></div>";
    return html;
}

static String htmlFooter()
{
    return String("</body></html>");
}

static void handleMainPage()
{
    String html = htmlHeader();
    html += "<div class='center'><div id='switch'>";
    if (device.powerIsOn()) {
        html += "<a class='state active' href='on'>ON</a>";
        html += "<a class='state' href='off'>OFF</a>";
    } else {
        html += "<a class='state' href='on'>ON</a>";
        html += "<a class='state active' href='off'>OFF</a>";
    }
    html += "</div></div><table><tr><th colspan='2'>Settings</th></tr>";
    //html += "<tr><td>Unique name</td><td>"+device.uniqueName()+"</td></tr>";
    html += "<tr><td>Device name</td><td>"+config.deviceAlias+"</td></tr>";
    html += "<tr><td>MAC address</td><td>"+device.macAddress()+"</td></tr>";
    html += "<tr><td>MQTT broker</td><td>"+config.mqttBroker+"</td></tr>";
    html += "<tr><td>MQTT topic</td><td>"+config.mqttTopic+"</td></tr>";
    html += "</table>";
    html += "<div id='footer'><a href='config.html'>Configure&nbsp;&rtrif;</a></div>";
    html += htmlFooter();
    webserver.send(200, "text/html", html);
}

static String inputField(String name, int length, String value)
{
    // String.replace() should return modified String, but is void. Crap!
    String val = value;
    val.replace("'", "&apos;");
    return "<input type='text' name='"+name+"' length="+String(length)+" value='"+val+"'>";
}

static void handleConfigPage()
{
    String html = htmlHeader();
    html += "<form method='get' action='submit.html'>";
    html += "<table><tr><th colspan='2'>Settings";
    html += "<tr><td><label>MQTT broker</label><td>"+inputField("mqttBroker", 32, config.mqttBroker);
    html += "<tr><td><label>MQTT topic</label><td>"+inputField("mqttTopic", 64, config.mqttTopic);
    html += "</table>";
    html += "<button type='submit' name='action' value='apply'>Apply</button>&nbsp;";
    html += "<button type='submit' name='action' value='cancel'>Cancel</button></form>";
    html += htmlFooter();
    webserver.send(200, "text/html", html);
}

static void handleOTADisabled()
{
    String html = htmlHeader();
    html += "<h1>OTA Update disabled!</h1>";
    html += htmlFooter();
    webserver.send(404, "text/html", html);
}

static void handleSubmitPage()
{
    bool apply = webserver.arg("action")!="cancel";

    if (apply) {
        String value = webserver.arg("mqttBroker");
        if (value.length()>0)
            config.mqttBroker = value;
        value = webserver.arg("mqttTopic");
        if (value.length()>0)
            config.mqttTopic = value;

        config.wifiSSID = WiFi.SSID();
        config.wifiPSK = WiFi.psk();
        saveConfig();
    }

    redirectHome();

    if (apply) {
        delay(500);
        ESP.restart();
    }
}

static void handleTurnOn()
{
    device.powerOn();
    redirectHome();
}

static void handleTurnOff()
{
    device.powerOff();
    redirectHome();
}

static void handleStylesheet()
{
    static const char *css =
"html {"
"    font-family: 'sans-serif';"
"}"
"a {"
"    outline: none;"
"    text-decoration: none;"
"    color: inherit;"
"}"
"table {"
"    border-collapse: collapse;"
"    width: 100%;"
"    background-color: #E2E7E8;"
"}"
"th, td {"
"    text-align: left;"
"    padding: 8px;"
"}"
"tr:nth-child(even) {"
"    background-color: #f2f2f2;"
"}"
"th {"
"    background-color: #869A9E;"
"    color: white;"
"}"
"div#header {"
"    display: block;"
"    padding: 5px;"
"    margin-bottom: 5px;"
"    font-size: 140%;"
"    background-color: #283593;"
"    color: white;"
"}"
"div#footer {"
"    background-color: #869A9E;"
"    color: #E2E7E8;"
"    width: 100%;"
"    text-align: right;"
"}"
"div#switch {"
"    display: inline-block;"
"    margin: 10px auto 10px auto;"
"    padding: .5em;"
"    background-color: #E5EAE3;"
"}"
"div.center {"
"    text-align: center;"
"}"
"button {"
"    display: inline-block;"
"    padding: .5em;"
"    margin-top: 5px;"
"    background-color: #17350B;"
"    color: white;"
"    border: none;"
"    font-size: 100%;"
"}"
"input[type=text] {"
"    border: none;"
"    font-size: 100%;"
"}"
".state {"
"    display: inline-block;"
"    width: 6em;"
"    padding: .5em;"
"    background-color: #E5EAE3;"
"    color: black;"
"    margin: 0;"
"    text-align: center;"
"    valign: center;"
"}"
".active {"
"    background-color: #17350B;"
"    color: white;"
"    font-weight: bold;"
"}"
    ;
    webserver.send(200, "text/css", css);
}

void setupWebServer()
{
    webserver.on("/", handleMainPage);
    webserver.on("/on", handleTurnOn);
    webserver.on("/off", handleTurnOff);
    webserver.on("/style.css", handleStylesheet);
    webserver.on("/config.html", handleConfigPage);
    webserver.on("/submit.html", handleSubmitPage);
/*
    webserver.onNotFound([]() {
        if (!handleFileRequest(webserver.uri()))
            webserver.send(404, "text/plain", "404: Not Found");
    });
*/
    webserver.begin();
    DEBUG_MSG("HTTP server started\n");

    // OTA Update
    MDNS.addService("http", "tcp", webserverPort);
    httpUpdater.setup(&webserver);
#if 0
    webserver.on("/update", handleOTADisabled);
#endif
}

void loopWebServer()
{
    webserver.handleClient();

#if 0
    // Doesn't work as intended ...
    if (otaOpen) {
        if (millis() - otaOpen >= otaOpenWindow) {
            webserver.on("/update", handleOTADisabled);
            otaOpen = false;
            DEBUG_MSG("OTA Update CLOSED\n");
        }
    }
#endif
}

void enableOTA()
{
#if 0
    otaOpen = true;
    otaOpenStart = millis();
    httpUpdater.setup(&webserver);
    DEBUG_MSG("OTA Update OPEN\n");
#endif
}

// vim: set et ts=4 sw=4:
