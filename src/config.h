#ifndef __CONFIG_H
#define __CONFIG_H

#include <Arduino.h>

void saveConfig();
bool setupConfig();
void eraseConfig();
void disableConfig();

typedef struct {
    String wifiSSID;
    String wifiPSK;
    String deviceAlias;
    String mqttBroker;
    String mqttTopic;
} config_t;

extern config_t config;

#endif

// vim: set et ts=4 sw=4:
