#ifndef __MUNIN_H
#define __MUNIN_H

void prepareMunin();
void setupMunin();
void loopMunin();

#endif

// vim: set et ts=4 sw=4:
