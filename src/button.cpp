#include <Ticker.h>
#include <ESP8266WiFi.h>

#include "sonoff_s20.h"
#include "button.h"
#include "utils.h"
#include "config.h"
#include "webserver.h"

static void buttonCallback()
{
    static byte count = 0;
    static byte state = 0;

    if (state==0 || state==1) {
        if (device.buttonIsPressed()) {
            if (count>=200)
                return;
            count += 1;

            if (count==3) {
                device.powerToggle();
            } else {
                if (count>50 && count<=100)
                    device.ledToggle();
                if (count==50)
                    state = 1;
                if (count==100) {
                    disableConfig();
                    state = 2;
                }
            }
        } else {
            if (state==1) {
                enableOTA();
                state = 0;
            }
            // FIXME: not on every tick, only after state change
            device.ledReset();
            count = 0;
        }
    } else if (state==2) {
        if (!device.buttonIsPressed()) {
            state = 0;
            ESP.restart();
        }
    }
}

void setupButton()
{
    static Ticker buttonChecker;

    buttonChecker.attach(0.1, buttonCallback);
}

void loopButton()
{
}

// vim: set et ts=4 sw=4:
