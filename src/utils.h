#ifndef __UTILS_H
#define __UTILS_H

#include <Arduino.h>

#ifdef DEBUG_ESP_PORT
#define DEBUG_MSG(...) DEBUG_ESP_PORT.printf( __VA_ARGS__ )
#else
#define DEBUG_MSG(...)
#endif

/**
    StreamReader class: Read characters from a stream into a line buffer.

    Beware: Does not handle Unicode (multibyte) characters!
*/
class StreamReader {
    public:
        StreamReader(unsigned int buffersize);
        StreamReader(Stream& stream, unsigned int buffersize);
        ~StreamReader();

        StreamReader& operator=(const StreamReader& other);
        StreamReader& operator<<(Stream& stream);

        String toString() const { return String(m_buffer); }
        const char *getBuffer() const { return m_buffer; }
        const unsigned int getLength() const { return m_pos==0 ? m_length : 0; }
        operator const char *() const { return m_buffer; }
        void flush() { m_pos = m_length = 0; }

        bool readLine();
        bool readTrimLine();
    private:
        Stream* m_stream;
        char *m_buffer;
        unsigned int m_buffersize;
        unsigned int m_pos;
        unsigned int m_length;
};

#endif

// vim: set et ts=4 sw=4:
