// Handle the hardware button

#ifndef __BUTTON_H
#define __BUTTON_H

void setupButton();
void loopButton();

#endif

// vim: set et ts=4 sw=4:
