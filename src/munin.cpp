#include <ESP8266WiFi.h>
#include "utils.h"
#include "config.h"
#include "sonoff_s20.h"

static String muninNodeId;
static const unsigned int muninNodePort = 4949;

static WiFiServer muninNodeServer(muninNodePort);

void prepareMunin()
{
}

void setupMunin()
{
    if (config.deviceAlias.length()==0)
        muninNodeId = device.uniqueName();
    else
        muninNodeId = config.deviceAlias;
    DEBUG_MSG("Munin node id: %s\n", muninNodeId.c_str());

    muninNodeServer.begin();
    muninNodeServer.setNoDelay(true);
    DEBUG_MSG("Munin node is running at %s:%d\n", WiFi.localIP().toString().c_str(), muninNodePort);
}

void loopMunin() {
    static WiFiClient client;
    static int state = 0;
    static unsigned long start = 0;
    static StreamReader reader(16);

    if (!client.connected()) {
        client = muninNodeServer.available();
        if (client.connected()) {
            state = 1;
            start = millis();
            DEBUG_MSG("Munin node: client connected\n");
            reader << client;
        }
    }

    if (client.connected()) {
        if (reader.readTrimLine()) {
            String command = reader.toString();
            DEBUG_MSG("Munin command received: %s\n", command.c_str());

            if (command == "quit") {
                client.stop();
                state = 0;
            } else if (command == "version") {
                client.println(String("munin node on ") + muninNodeId + String(" version: 1.0.0"));
            } else if (command == "list") {
                client.println(String("state"));
            } else if (command == "config state") {
                client.println("graph_title Outlet");
                client.println("graph_args --lower-limit 0 --upper-limit 1 --base 1");
                client.println("graph_scale no");
                client.println("graph_vlabel state");
                client.println("graph_category power");
                client.println("state.label Outlet state");
                client.println(".");
            } else if (command == "fetch state") {
                int value = device.powerIsOn() ? 1 : 0;
                client.println("state.value " + String(value));
                client.println(".");
            } else {
                client.println("# Unknown command. Try list, config, fetch, version or quit");
            }
            start = millis();  // reset timeout check
        } else {
            if (millis() - start >= 5000) {
                client.println("# Disconnect due to timeout");
                client.stop();
                state = 0;
            }
        }
    } else {
        if (state>0) {
            state = 0;
            DEBUG_MSG("Munin node: client dropped connection");
            client.stop();
        }
    }
}

// vim: set et ts=4 sw=4:
