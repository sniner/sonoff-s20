#include "utils.h"

StreamReader::StreamReader(unsigned int buffersize)
{
    m_stream = NULL;
    m_buffersize = buffersize<=1 ? 32 : buffersize;
    m_buffer = (char *)malloc(buffersize);
    if (m_buffer==NULL) {
        m_buffersize = 0;
        DEBUG_MSG("StreamReader(%u): Not enough memory\n", buffersize);
    }
    m_pos = m_length = 0;
}

StreamReader::StreamReader(Stream& stream, unsigned int buffersize)
    : StreamReader(buffersize)
{
    m_stream = &stream;
}

StreamReader::~StreamReader()
{
    if (m_buffer)
       free(m_buffer);
}

StreamReader& StreamReader::operator=(const StreamReader& other)
{
    m_stream = other.m_stream;
    flush();
}

StreamReader& StreamReader::operator<<(Stream& stream)
{
    m_stream = &stream;
    flush();
}

bool StreamReader::readLine()
{
    if (m_buffersize==0)
        return false;

    while (m_stream->available()) {
        byte ch = m_stream->read();

        if (ch == '\n') {
            m_buffer[m_pos] = '\0';
            m_length = m_pos>0 ? m_pos-1 : 0;
            m_pos = 0;
            return true;
        }

        if (ch >= 32 && m_pos < m_buffersize-1) {
            m_buffer[m_pos++] = ch;
        }
    }
    return false;
}

bool StreamReader::readTrimLine()
{
    if (m_buffersize==0)
        return false;

    while (m_stream->available()) {
        byte ch = m_stream->read();

        if (ch == '\n') {
            // remove trailing spaces
            while (m_pos>0 && isspace(m_buffer[m_pos-1]))
                --m_pos;
            m_buffer[m_pos] = '\0';
            m_length = m_pos>0 ? m_pos-1 : 0;
            m_pos = 0;
            return true;
        }

        // convert control characters to a blank
        if (ch < 32)
            ch = ' ';

        bool space = isspace(ch);

        // skip leading spaces
        if (space && m_pos == 0)
            continue;

        if (m_pos < m_buffersize-1) {
            // collapse multiple spaces to a single blank
            if (!space || (m_pos>0 && !isspace(m_buffer[m_pos-1])))
                m_buffer[m_pos++] = ch;
        }
    }
    return false;
}

// vim: set et ts=4 sw=4:
