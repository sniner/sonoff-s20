#ifndef __WIFI_H
#define __WIFI_H

void setupWifi();
void loopWifi();

#endif

// vim: set et ts=4 sw=4:
