#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include <WiFiManager.h>

#include "config.h"
#include "utils.h"
#include "sonoff_s20.h"

config_t config;

static const char* eepromHeader = "s9r\0";
static const int eepromSize = 512;

static bool isFormatted()
{
    bool b = true;
    int len = strlen(eepromHeader);
    for (int pos=0;pos<len; ++pos) {
        b &= EEPROM.read(pos)==eepromHeader[pos];
    }
    return b;
}

// Mode:
//   0: config valid and populated
//   1: reconfiguration requested, but config is populated
//   2: config is to be ignored
static int getMode()
{
    int len = strlen(eepromHeader);
    return EEPROM.read(len);
}

static void setMode(byte mode)
{
    int len = strlen(eepromHeader);
    EEPROM.write(len, mode);
}

static int format()
{
    int len = strlen(eepromHeader)+1;
    for (int i=0; i<len; ++i)
        EEPROM.write(i, eepromHeader[i]);
    return len;
}

static void erase(int ofs, unsigned int bytes)
{
    for (int i=0; i<bytes; i++)
        EEPROM.write(ofs+i, 0);
}

static String loadString(int& ofs)
{
    int i, len;
    // determine string length
    for (len=0;;++len) {
        byte ch = EEPROM.read(ofs+len);
        if (ch==0 || ofs+len>=eepromSize)
            break;
    }
    // allocate temporary memory
    char* str = (char*)malloc(len+1);
    // get string
    for (i=0; i<=len; ++i) {
        str[i] = EEPROM.read(ofs+i);
    }
    String result = String(str);
    // release temporary memory
    free(str);
    // advance offset
    ofs += len+1;
    return result;
}

static void writeString(int& ofs, const String& str)
{
    int len = str.length();
    const char *p = str.c_str();

    for (int i=0;i<len;++i) {
        EEPROM.write(ofs+i, *p++);
    }
    EEPROM.write(ofs+len, 0);
    ofs += len+1;
}

static bool loadConfig()
{
    EEPROM.begin(eepromSize);

    bool valid = isFormatted();
    int mode = -1;

    if (valid) {
        mode = getMode();
        int ofs = strlen(eepromHeader)+1;
        if (mode==0 || mode==1) {
            DEBUG_MSG("Restoring configuration\n");
            config.wifiSSID = loadString(ofs);
            config.wifiPSK = loadString(ofs);
            config.deviceAlias = loadString(ofs);
            config.mqttBroker = loadString(ofs);
            config.mqttTopic = loadString(ofs);
        } else {
            DEBUG_MSG("Default configuration in use\n");
        }
        if (config.deviceAlias.length()==0)
            config.deviceAlias = device.uniqueName();
    } else {
        DEBUG_MSG("No configuration found\n");
    }
    return mode==0;
}

void saveConfig()
{
    DEBUG_MSG("Saving configuration\n");
    int ofs = format();
    writeString(ofs, config.wifiSSID);
    writeString(ofs, config.wifiPSK);
    writeString(ofs, config.deviceAlias);
    writeString(ofs, config.mqttBroker);
    writeString(ofs, config.mqttTopic);
    erase(ofs, 16);
    EEPROM.commit();
}

void eraseConfig()
{
    int len = strlen(eepromHeader)+1;
    for (int i=0; i<len; ++i)
        EEPROM.write(i, 0);
    EEPROM.commit();
}

void disableConfig()
{
    setMode(1);
    EEPROM.commit();
}

bool setupConfig()
{
    if (!loadConfig()) {
        DEBUG_MSG("Starting WiFiManager\n");
        WiFiManager wifiManager;
#if 0
        WiFiManagerParameter customAdminPassword("adminPassword", "Admin password", config.adminPassword.c_str(), 16);
        wifiManager.addParameter(&customAdminPassword);
#endif
        WiFiManagerParameter customDeviceAlias("deviceAlias", "Unique device name", config.deviceAlias.c_str(), 24);
        wifiManager.addParameter(&customDeviceAlias);
        if (wifiManager.startConfigPortal(device.uniqueName().c_str())) {
            config.wifiSSID = WiFi.SSID();
            config.wifiPSK = WiFi.psk();
#if 0
            config.adminPassword = customAdminPassword.getValue();
#endif
            config.deviceAlias = customDeviceAlias.getValue();
            saveConfig();
        } else {
            ESP.restart();
        }
        return false;
    }
    return true;
}

// vim: set et ts=4 sw=4:
