#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <PubSubClient.h>

#include "config.h"
#include "sonoff_s20.h"
#include "mqtt.h"
#include "utils.h"

static const int mqttPort = 1883;

static String mqttClientName;
static String mqttPubTopic;

static WiFiClient mqttWifiClient;
static PubSubClient mqttClient(mqttWifiClient);

static bool mqttEnabled = true;

static void mqttCallback(char* topic, byte* payload, unsigned int length)
{
    if (length<1)
        return;

    char buf[8];
    unsigned int i = 0;
    unsigned int len = length<8 ? length : 7;
    while (i<len) {
        buf[i] = (char)payload[i];
        i += 1;
    }
    buf[i] = '\0';

    DEBUG_MSG("MQTT topic '%s' received with payload '%s'\n", topic, buf);

    String value = String(buf);
    if (value.equalsIgnoreCase("ON")) {
        device.powerOn();
    } else if (value.equalsIgnoreCase("OFF")) {
        device.powerOff();
    } else if (value.equalsIgnoreCase("TOGGLE")) {
        device.powerToggle();
    } else {
        DEBUG_MSG("MQTT payload unrecognized\n");
    }
}

void prepareMQTT()
{
    mqttClientName = SonoffS20::getInstance().uniqueName();
    // Set default values
    config.mqttTopic = mqttClientName+"/switch";
}

void setupMQTT()
{
    // NB: config has to be populated
    if (config.deviceAlias.length()>0)
        mqttClientName = config.deviceAlias;
    else
        mqttEnabled = false;

    if (config.mqttTopic.length()==0 || config.mqttBroker.length()==0)
        mqttEnabled = false;

    mqttPubTopic = config.mqttTopic+"/status";

    DEBUG_MSG("MQTT broker: %s\n", config.mqttBroker.c_str());
    DEBUG_MSG("MQTT device name: %s\n", mqttClientName.c_str());
    DEBUG_MSG("MQTT sub topic: %s\n", config.mqttTopic.c_str());
    DEBUG_MSG("MQTT pub topic: %s\n", mqttPubTopic.c_str());

    if (mqttEnabled) {
        mqttClient.setServer(config.mqttBroker.c_str(), mqttPort);
        mqttClient.setCallback(mqttCallback);
    }
}

static void mqttPublishState(bool force = false)
{
    static int savedState = -1;
    int currentState = device.powerIsOn() ? 1 : 0;

    if (savedState != currentState || force) {
        savedState = currentState;
        if (mqttEnabled) {
            if (currentState==1) {
                mqttClient.publish(mqttPubTopic.c_str(), "ON", true);
                DEBUG_MSG("MQTT publish: ON\n");
            } else {
                mqttClient.publish(mqttPubTopic.c_str(), "OFF", true);
                DEBUG_MSG("MQTT publish: OFF\n");
            }
        }
    }
}

void loopMQTT()
{
    if (!mqttEnabled)
        return;

    if (mqttClient.connected()) {
        mqttPublishState();
        mqttClient.loop();
    } else {
        static unsigned long mqtt_last_reconnect = 0;
        unsigned long now = millis();
        if (now - mqtt_last_reconnect >= 5000) {
            mqtt_last_reconnect = now;

            DEBUG_MSG("Connect to MQTT-Broker");
            if (mqttClient.connect(mqttClientName.c_str())) {
                DEBUG_MSG(" succeeded.\n");
                mqttClient.subscribe(config.mqttTopic.c_str());
                mqttPublishState(true);
            } else {
                DEBUG_MSG(" failed: %s\n", mqttClient.state());
            }
        }
    }
}

// vim: set et ts=4 sw=4 ft=cpp:
