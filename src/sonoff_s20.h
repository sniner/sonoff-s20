#ifndef __SONOFF_S20_H
#define __SONOFF_S20_H

class SonoffS20
{
private:
    static const int GPIO_LED = 13;
    static const int GPIO_RELAY = 12;
    static const int GPIO_BUTTON = 0;

    static const int RELAY_OFF = LOW;
    static const int RELAY_ON = HIGH;

    static const int LED_OFF = HIGH;
    static const int LED_ON = LOW;

public:
    static SonoffS20& getInstance() { static SonoffS20 instance; return instance; }
    SonoffS20(SonoffS20 const&) = delete;
    void operator=(SonoffS20 const&) = delete;

    String uniqueName() const { return m_uniqueName; }
    String macAddress() const { return m_macAddress; }

    void setup();

    void powerOn();
    void powerOff();
    void powerToggle();
    bool powerIsOn();
    bool powerIsOff();

    bool buttonIsPressed();

    void ledOn();
    void ledOff();
    void ledToggle();
    void ledReset();
    bool ledIsOn();

private:
    SonoffS20();

    String m_macAddress;
    String m_uniqueName;
};

extern SonoffS20& device;

#endif

// vim: set et ts=4 sw=4:
