#include <ESP8266WiFi.h>

#include "sonoff_s20.h"
#include "utils.h"

SonoffS20& device = SonoffS20::getInstance();

bool SonoffS20::powerIsOn()
{
    return digitalRead(GPIO_RELAY)==RELAY_ON;
}

bool SonoffS20::powerIsOff()
{
    return !powerIsOn();
}

void SonoffS20::powerOn()
{
    if (powerIsOff()) {
        DEBUG_MSG("Switching ON\n");
        digitalWrite(GPIO_LED, LED_ON);
        digitalWrite(GPIO_RELAY, RELAY_ON);
        delay(1000);
    }
}

void SonoffS20::powerOff()
{
    if (powerIsOn()) {
        DEBUG_MSG("Switching OFF\n");
        digitalWrite(GPIO_LED, LED_OFF);
        digitalWrite(GPIO_RELAY, RELAY_OFF);
        delay(1000);
    }
}

void SonoffS20::powerToggle()
{
    if (powerIsOn())
        powerOff();
    else
        powerOn();
}

bool SonoffS20::ledIsOn()
{
    return digitalRead(GPIO_LED)==LED_ON;
}

void SonoffS20::ledOn()
{
    digitalWrite(GPIO_LED, LED_ON);
}

void SonoffS20::ledOff()
{
    digitalWrite(GPIO_LED, LED_OFF);
}

void SonoffS20::ledToggle()
{
    if (ledIsOn())
        ledOff();
    else
        ledOn();
}

void SonoffS20::ledReset()
{
    if (powerIsOn())
        ledOn();
    else
        ledOff();
}

void SonoffS20::setup()
{
    pinMode(GPIO_LED, OUTPUT);
    pinMode(GPIO_RELAY, OUTPUT);
    digitalWrite(GPIO_LED, LED_OFF);
    digitalWrite(GPIO_RELAY, RELAY_OFF);
}

bool SonoffS20::buttonIsPressed()
{
    return digitalRead(GPIO_BUTTON)==LOW;
}

SonoffS20::SonoffS20()
{
    // Build unique device name
    int i = 0;
    byte mac[6];
    char buf[13], *p = buf;
    WiFi.macAddress(mac);
    while (i<6) {
        sprintf(p, "%02x", mac[i]);
        p += 2;
        i += 1;
    }
    m_macAddress = String(buf);
    m_uniqueName = String("sonoff-s20-")+m_macAddress;
}

// vim: set et ts=4 sw=4:
