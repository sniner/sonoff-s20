#ifndef __MQTT_H
#define __MQTT_H

void prepareMQTT();
void setupMQTT();
void loopMQTT();

#endif

// vim: set et ts=4 sw=4:
