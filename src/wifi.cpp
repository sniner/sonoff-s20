#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>

#include "wifi.h"
#include "sonoff_s20.h"
#include "utils.h"
#include "config.h"

void setupWifi()
{
    WiFi.persistent(false);
    WiFi.mode(WIFI_STA);
    WiFi.begin(config.wifiSSID.c_str(), config.wifiPSK.c_str());

    DEBUG_MSG("Configured WiFi AP: %s\n", config.wifiSSID.c_str());
    while (WiFi.status() != WL_CONNECTED) {
        DEBUG_MSG(".");
        device.ledToggle();
        delay(500);
    }
    DEBUG_MSG("Connected! IP address: %s\n", WiFi.localIP().toString().c_str());
    device.ledReset();
}

void loopWifi()
{
}

// vim: set et ts=4 sw=4:
